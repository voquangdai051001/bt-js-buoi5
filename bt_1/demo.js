function khuvuc(vung) {
  var diemkhuvuc = "";
  if (vung == "A") {
    diemkhuvuc = 2;
  } else if (vung == "B") {
    diemkhuvuc = 1;
  } else if (vung == "C") {
    diemkhuvuc = 0.5;
  } else if (vung == "X") {
    diemkhuvuc = 0;
  }
  return diemkhuvuc;
}

function doituongthi(doituong) {
  var diemdoituong = "";
  if (doituong == "1") {
    diemdoituong = 2.5;
  } else if (doituong == "2") {
    diemdoituong = 1.5;
  } else if (doituong == "3") {
    diemdoituong = 1;
  } else if (doituong == "0") {
    diemdoituong = 0;
  }
  return diemdoituong;
}

function ketqua() {
  var diemchuan = document.getElementById("txt_diemchuan").value;

  var diemthunhat = document.getElementById("txt_monthunhat").value * 1;

  var diemthuhai = document.getElementById("txt_monthuhai").value * 1;

  var diemthuba = document.getElementById("txt_monthuba").value * 1;

  var e = document.getElementById("MySelectOption1");
  var strUser = e.options[e.selectedIndex].value * 1;
  console.log("strUser: ", strUser);

  var e1 = document.getElementById("MySelectOption2");
  var strUser1 = e1.options[e1.selectedIndex].value * 1;
  console.log("strUser1: ", strUser1);

  var tongdiem = diemthuba + diemthuhai + diemthunhat + strUser + strUser1;
  console.log("tongdiem: ", tongdiem);

  if (
    tongdiem >= diemchuan &&
    diemthunhat > 0 &&
    diemthuhai > 0 &&
    diemthuba > 0
  ) {
    document.getElementById("result").innerText = ` Điểm: ${tongdiem}.  Đạt`;
  } else {
    document.getElementById("result").innerText = ` Điểm: ${tongdiem}.  Rớt`;
  }
}
