function tinhtien() {
  var soKwtieuthu = document.getElementById("txt_sokw").value * 1;
  var tennguoidung = document.getElementById("txt_ten").value;

  var td = sokw(soKwtieuthu);

  var tiendien = new Intl.NumberFormat().format(td);

  document.getElementById("result").innerText = ` Người dùng: ${tennguoidung}.
   Tiền điện: ${tiendien} vnđ`;
}

function sokw(sokw) {
  var tongtien = 0;
  if (sokw <= 50) {
    tongtien = sokw * 500;
  } else if (sokw <= 100) {
    tongtien = 50 * 500 + (sokw - 50) * 650;
  } else if (sokw <= 200) {
    tongtien = 50 * 500 + 50 * 650 + (sokw - 100) * 850;
  } else if (sokw <= 350) {
    tongtien = 50 * 500 + 50 * 650 + 100 * 850 + (sokw - 200) * 1100;
  } else {
    tongtien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (sokw - 350) * 1300;
  }
  return tongtien;
}
